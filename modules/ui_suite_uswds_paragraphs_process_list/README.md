# UI Suite USWDS Paragraphs Process List

Based on https://designsystem.digital.gov/components/process-list/

## Includes

Ships with 2 paragraph bundles `Process List` and `Process List item`

### Process List fields

* Items
  * Paragraph reference to Process List item bundle
  * Not using a twig template but Rendered Entity wrapped in Pattern

### Process List item fields

* Header - self-explanatory
  * No-markup used
* Content - self-explanatory

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_process_list_item/fields/paragraph.ui_suite_uswds_process_list_item.field_process_list_item_content`

