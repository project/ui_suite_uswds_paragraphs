# UI Suite USWDS Paragraphs Modal

Based on https://designsystem.digital.gov/components/modal/

## Includes

Ships with a single paragraph bundles `Modal`

### Modal fields

* Button Text = self-explanatory
  * No-markup used
* Content = self-explanatory
* Forced action = Hides the X button and forces user to select Yes/No buttons.
  * Boolean field
* Large = Should the modal appear "Large"
  * Boolean field
* No link = link used by "No" button
* No text = text for the "No" button
* Title = self-explanatory
  * No-markup used
* Yes link = link used by "Yes" button
* Yes text = text for the "Yes" button

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_modal/fields/paragraph.ui_suite_uswds_modal.field_modal_content`

