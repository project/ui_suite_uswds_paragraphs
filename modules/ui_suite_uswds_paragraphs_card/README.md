# UI Suite USWDS Paragraphs Card

Based on https://designsystem.digital.gov/components/card/

## Includes

Ships with 3 paragraph bundles `Card Group`, `Card Single`, and `Card`

### Card Group fields

* Cards
  * Paragraph reference to Card bundle

### Card Single fields

* Card
  * Paragraph reference to Card bundle

### Card fields

* Body Exdent = Extends the body element out over the card border. Useful for light-bordered cards.
  * Boolean field
* Content = self-explanatory
* Footer = self-explanatory
  * No-markup used
* Footer Exdent = Extends the footer element out over the card border. Useful for light-bordered cards.
  * Boolean field
* Footer URL = self-explanatory
* Header = self-explanatory
  * No-markup used
* Header Exdent = Extends the header element out over the card border. Useful for light-bordered cards.
  * Boolean field
* Header first = Make the header/title appear above image.
  * Boolean field
* Make Flag = Turns the card into a flag card.
  * Boolean field
* Media Inset = Indents the media element so it doesn’t extend to the edge of the card.
  * Boolean field
* Media Exdent = Extends the media element out over the card border. Useful for light-bordered cards.
  * Boolean field
* Media Inset = Indents the media element so it doesn’t extend to the edge of the card.
  * Boolean field
* Desktop = 1-12 breakpoint
* Tablet = 1-12 breakpoint

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_card/fields/paragraph.ui_suite_uswds_card.field_card_content`
