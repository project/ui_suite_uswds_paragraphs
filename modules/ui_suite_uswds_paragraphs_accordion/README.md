# UI Suite USWDS Paragraphs Accordion

Based on https://designsystem.digital.gov/components/accordion/

## Includes

Ships with 2 paragraph bundles `Accordion` and `Accordion Item`

### Accordion fields

* Accordion Sections
  * Paragraph reference to Accordion Item bundle
* Bordered = Add a border around the accordion content.
  * Boolean field
* Default Open = Default one of the accordions to open.
  * Number field
* Heading level = Useful for accessibility.
  * List field
* Multiselect = Allow more than one accordion to be open at once.
  * Boolean field

### Accordion Item fields

* Title = Accordion button text.
  * No-markup used.dd
* Body = Content of accordion.
