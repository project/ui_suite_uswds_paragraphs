# UI Suite USWDS Paragraphs Icon List

Based on https://designsystem.digital.gov/components/icon-list/

## Includes

Ships with 2 paragraph bundles `Icon list` and `Icon list Item`

### Icon list fields

* Items
  * Paragraph reference to Icon list Item bundle
  * Not using a twig template but Rendered Entity wrapped in Pattern

### Icon list item fields

* Header
  * No-markup used
* Content = self-explanatory
* Icon
  * List filed
  * Only added a few icons vs the 200+ available but if more are wanted all from https://designsystem.digital.gov/components/icon/ should work with `Icon` pattern

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_alert/fields/paragraph.ui_suite_uswds_alert.field_alert_body`

