# UI Suite USWDS Paragraphs Summary Box

Based on https://designsystem.digital.gov/components/summary-box/

## Includes

Ships with a single paragraph bundles `Summary box`

### Summary box fields

* Heading = Summary header
  * No-markup used
* Content = See recommended settings below

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_summary_box/fields/paragraph.ui_suite_uswds_summary_box.field_summary_box_content` and
`admin/structure/paragraphs_type/ui_suite_uswds_summary_box/fields/paragraph.ui_suite_uswds_summary_box.field_summary_box_heading`

