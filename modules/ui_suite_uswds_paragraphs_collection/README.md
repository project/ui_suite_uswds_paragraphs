# UI Suite USWDS Paragraphs Collections

Based on https://designsystem.digital.gov/components/collection/

## Includes

Ships with 2 paragraph bundles `Collection` and `Collection item`

### Collection fields

* Items
  * Paragraph reference to Collection Item bundle
  * Not using a twig template but Rendered Entity wrapped in Pattern

### Collection item fields

* Header
  * No-markup used
* Content
* Meta Items
  * list field (text)
* Meta Tags
  * Entity reference to "Tags" taxonomy
  * Link to content enabled

## Recommended settings
After installing recommend configuring allowed text formats at
`/admin/structure/paragraphs_type/ui_suite_uswds_collection_item/fields/paragraph.ui_suite_uswds_collection_item.field_collection_item_content`
