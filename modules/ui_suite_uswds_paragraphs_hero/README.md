# UI Suite USWDS Paragraphs Hero

Based on banner at https://federalist-3b6ba08e-0df4-44c9-ac73-6fc193b0e19c.sites.pages.cloud.gov/preview/uswds/uswds/develop/iframe.html?id=pages-landing-page--landing-page&args=&viewMode=story

## Includes

Ships with a single paragraph bundles `Hero banner`

### Hero banner fields

* Title
  * No-markup used
* Title Callout = appears above the title
  * No-markup used
* Body = self-explanatory
* Background Image
  * Image field
  * Image style wide

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_hero/fields/paragraph.ui_suite_uswds_hero.field_hero_body`

