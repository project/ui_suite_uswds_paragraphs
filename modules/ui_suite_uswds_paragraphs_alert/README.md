# UI Suite USWDS Paragraphs Alert

Based on https://designsystem.digital.gov/components/alert/

## Includes

Ships with a single paragraph bundles `Alert`

### Alert fields

* Heading = Alert heading (self-explanatory)
  * No-markup used
* Body = Alert body (self-explanatory)
* No Icon = Determines if alert icons appears.
  * Boolean field
* Slim = Makes the alert slightly slimmer.
  * Boolean field
* Type = Determines what color alert will appear as.
  * List field
  * Required

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_alert/fields/paragraph.ui_suite_uswds_alert.field_alert_body`

