# UI Suite USWDS Paragraphs Step Indicator

Based on https://designsystem.digital.gov/components/step-indicator/

## Includes

Ships with a single paragraph bundles `Step indicator`

### Summary box fields

* Heading Level = Useful for accessibility.
  * Defaults to H2
* Variant = Type of step indicator that will render
  * List field
* Label = self-explanatory
  * No-markup used
* In progress Items
  * List field
  * No-markup used
* Completed Items
  * List field
  * No-markup used

## Recommended settings
After installing recommend configuring allowed text formats at
`admin/structure/paragraphs_type/ui_suite_uswds_summary_box/fields/paragraph.ui_suite_uswds_summary_box.field_summary_box_content` and
`admin/structure/paragraphs_type/ui_suite_uswds_summary_box/fields/paragraph.ui_suite_uswds_summary_box.field_summary_box_heading`

